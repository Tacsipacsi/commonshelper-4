<?PHP
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once '/data/project/magnustools/public_html/php/Widar.php' ;
$widar = new \Widar ( 'commonshelper' ) ;
$widar->attempt_verification_auto_forward ( '/' ) ;
$widar->authorization_callback = 'https://commonshelper.toolforge.org/api.php' ;
if ( $widar->render_reponse(true) ) exit(0);

?>